# Nike Interview Frontend Code Challenge [Angular]

## Start the project

- Install dependencies: `npm i`
- (Optional) Install Angular CLI globally: `npm i @angular/cli@latest -g`
- Start Angular in development mode: `ng serve` (with Angular CLI installed globally) or `npm start`

## Project Description

### Changes & Improvements

- Encapsulated shoe-related features into its own feature module. Added routing to support lazy-loading of feature modules.
- Created dedicated HTTP service to handle backend calls.
- Built the Angular application in "reactive" way by utilizing `Observable`s and the `async` pipe. All manual subscriptions in components are removed.
- Added tests to test the critical business requirements of the frontend, which is to display the correct action string based on the current price and the min/max price of shoes.
- Upgrade the main layout method from `table` to CSS grid.
- Added price-refreshing capability. Now you can click on each row and get the most updated price for each shoe model. The action string will update accordingly.

### Potential Improvements

- Configure `commitlint` to standardize commit message.
- Include `eslint` and corresponding plugins for Angular and RxJS.
- Add scripts to automate linting and testing, so that they can be included in CI pipelines.

### Doubts & Assumptions

- The new price API returns two prices, the original price and the final discounted price. I am confused about which one should be treated as the "current price" used in the comparison with min/max price. I **assumed** that the **original** price should be used.

- In the original setup, the shoes "database" was located in the frontend. It confused me quite a bit and I finally decided to let backend handle the single source of truth. The initial shoes list is now fetched from the backend.
