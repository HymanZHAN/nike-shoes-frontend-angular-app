import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Shoe } from './shoe';

import { ShoeRowComponent } from './shoe-row.component';
import { ShoesService } from './shoes.service';

const actionStringForHighPrice = 'Can wait for discount';
const actionStringForMedPrice = 'Moderate state, can buy now!';
const actionStringForLowPrice = 'Best time to buy!';

const testShoe: Shoe = {
  id: '1',
  maxPrice: 190,
  minPrice: 90,
  model: 'Nike Test',
};

const testCheapShoe: Shoe = {
  id: '1',
  maxPrice: 100,
  minPrice: 60,
  model: 'Nike Test',
};

const testExpensiveShoe: Shoe = {
  id: '1',
  maxPrice: 250,
  minPrice: 200,
  model: 'Nike Test',
};

class MockShoesService {
  refreshPrice(id: number | string) {
    return of(180);
  }
}

describe('ShoeRowComponent (class only)', () => {
  let fixture: ComponentFixture<ShoeRowComponent>;
  let component: ShoeRowComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShoeRowComponent],
      providers: [{ provide: ShoesService, useClass: MockShoesService }],
    }).compileComponents();

    fixture = TestBed.createComponent(ShoeRowComponent);
    component = fixture.componentInstance;
  });

  it('should have correct action string - cheap shoes', (done: DoneFn) => {
    component.shoe = testCheapShoe;
    component.refreshPrice();
    fixture.detectChanges();

    component.actionString$.subscribe({
      next: (actionString) => {
        expect(actionString)
          .withContext('expected action string')
          .toEqual(actionStringForHighPrice);
        done();
      },
      error: done.fail,
    });
  });

  it('should have correct action string - moderate priced shoes', (done: DoneFn) => {
    component.shoe = testShoe;
    component.ngOnChanges();

    fixture.detectChanges();

    component.actionString$.subscribe({
      next: (actionString) => {
        expect(actionString)
          .withContext('expected action string')
          .toEqual(actionStringForMedPrice);
        done();
      },
      error: done.fail,
    });
  });

  it('should have correct action string - expensive shoes', (done: DoneFn) => {
    component.shoe = testExpensiveShoe;
    component.refreshPrice();

    fixture.detectChanges();

    component.actionString$.subscribe({
      next: (actionString) => {
        expect(actionString)
          .withContext('expected action string')
          .toEqual(actionStringForLowPrice);
        done();
      },
      error: done.fail,
    });
  });
});
