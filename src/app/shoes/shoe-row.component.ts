import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { EMPTY_SHOE, Shoe } from './shoe';
import { ShoesService } from './shoes.service';

@Component({
  selector: 'app-shoe-row',
  template: `
    <div (click)="refreshPrice()">
      <span>{{ shoe.model }}</span>
      <span>{{ price$ | async | currency }}</span>
      <span>{{ actionString$ | async }}</span>
    </div>
  `,
  styles: [
    `
      div {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        margin-top: 10px;
        margin-bottom: 10px;
        cursor: pointer;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShoeRowComponent implements OnChanges {
  @Input() shoe: Shoe = EMPTY_SHOE;

  price$!: Observable<number>;
  actionString$ = new BehaviorSubject<string>('');

  constructor(private shoeService: ShoesService) {}

  ngOnChanges(): void {
    this.refreshPrice();
  }

  refreshPrice() {
    this.price$ = this.shoeService.refreshPrice(this.shoe.id).pipe(
      tap((price) => {
        console.log(price);
        let actionString = '';
        if (price > this.shoe.maxPrice) {
          actionString = 'Can wait for discount';
        } else if (price < this.shoe.minPrice) {
          actionString = 'Best time to buy!';
        } else {
          actionString = 'Moderate state, can buy now!';
        }
        this.actionString$.next(actionString);
      })
    );
  }
}
