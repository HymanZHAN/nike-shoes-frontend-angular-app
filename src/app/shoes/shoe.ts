export interface Shoe {
  id: string;
  model: string;
  minPrice: number;
  maxPrice: number;
}

export interface ShoePrices {
  originalPrice: number;
  finalPrice: number;
}

export const EMPTY_SHOE: Shoe = {
  id: '',
  model: '',
  minPrice: 0,
  maxPrice: 0,
};
