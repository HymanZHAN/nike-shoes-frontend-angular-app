import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ShoesService } from './shoes.service';

@Component({
  selector: 'app-shoes',
  templateUrl: './shoes.component.html',
  styles: [
    `
      .wrapper {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
      }
      .wrapper .header {
        font-size: 1.1rem;
        font-weight: 600;
      }
      .wrapper .detail {
        grid-column: 1 / 4;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShoesComponent {
  constructor(public shoeService: ShoesService) {}
}
