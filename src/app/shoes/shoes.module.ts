import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ShoesRoutingModule } from './shoes-routing.module';
import { ShoesComponent } from './shoes.component';
import { ShoesService } from './shoes.service';
import { ShoeRowComponent } from './shoe-row.component';

@NgModule({
  declarations: [ShoesComponent, ShoeRowComponent],
  imports: [CommonModule, ShoesRoutingModule, HttpClientModule],
  providers: [ShoesService],
})
export class ShoesModule {}
