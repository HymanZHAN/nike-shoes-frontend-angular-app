import { HttpClient } from '@angular/common/http';
import { fakeAsync, tick } from '@angular/core/testing';
import { defer } from 'rxjs';

import { Shoe, ShoePrices } from './shoe';
import { ShoesService } from './shoes.service';

describe('ShoesService', () => {
  let service: ShoesService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  const expectedShoes: Shoe[] = [
    { id: '1', maxPrice: 200, minPrice: 100, model: 'Nike Test A' },
    { id: '2', maxPrice: 200, minPrice: 100, model: 'Nike Test A' },
    { id: '3', maxPrice: 200, minPrice: 100, model: 'Nike Test A' },
  ];
  const expectedPrices: ShoePrices = {
    originalPrice: 200,
    finalPrice: 200 * 0.6,
  };

  beforeEach(fakeAsync(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    httpClientSpy.get.and.returnValue(asyncData(expectedShoes));
    service = new ShoesService(httpClientSpy);
    tick();
  }));

  it('should fetch shoes list on creation', fakeAsync(() => {
    let fetchedShoes: Shoe[] = [];

    service.shoes$.subscribe((shoes) => (fetchedShoes = shoes));

    expect(fetchedShoes)
      .withContext('expected shoes list')
      .toEqual(expectedShoes);
  }));

  it('should refresh price', fakeAsync(() => {
    httpClientSpy.get.and.returnValue(asyncData(expectedPrices));
    let fetchedPrices = 0;

    service.refreshPrice('1').subscribe((prices) => (fetchedPrices = prices));
    tick();

    expect(fetchedPrices)
      .withContext('expected shoe price')
      .toEqual(expectedPrices.originalPrice);
  }));
});

function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}
