import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, map, Subject, takeUntil, tap } from 'rxjs';
import { Shoe, ShoePrices } from './shoe';

@Injectable()
export class ShoesService implements OnDestroy {
  private shoesUrl = `/api/shoes`;
  private shoePriceUrl = `/api/shoe-price`;

  private shoes = new BehaviorSubject<Shoe[]>([]);
  shoes$ = this.shoes.asObservable();

  private destroy$ = new Subject<void>();

  constructor(private http: HttpClient) {
    this.http
      .get<Shoe[]>(`${this.shoesUrl}`)
      .pipe(
        tap((shoes) => this.shoes.next(shoes)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  refreshPrice(id: number | string) {
    // TODO: originalPrice or finalPrice?
    return this.http
      .get<ShoePrices>(`${this.shoePriceUrl}/${id}`)
      .pipe(map((prices) => prices.originalPrice));
  }
}
